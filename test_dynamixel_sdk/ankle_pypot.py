#!/usr/bin/env python
# coding: utf-8

import pypot.robot
import time,math

nugget = pypot.robot.from_json('e_nugget.json')

nugget.r_ankle_y.compliant = False
for i in range(1000):
    t = time.time()
    nugget.r_ankle_y.goal_position = 20*math.sin(2*math.pi*0.5*t)
    while time.time()-t<0.01:
        time.sleep(0.001)
nugget.r_ankle_y.compliant = True

nugget.close()