#!/usr/bin/env python
# coding: utf-8

from dynamixel_sdk import *
import time,math
# Control table address
ADDR_AX_TORQUE_ENABLE      = 24
ADDR_AX_GOAL_POSITION      = 30
ADDR_AX_PRESENT_POSITION   = 36
ADDR_AX_PRESENT_TEMPERATURE   = 43
PROTOCOL_VERSION            = 1.0
DXL_ID                      = 17
BAUDRATE                    = 1000000
DEVICENAME                  = '/dev/ttyACM0'
TORQUE_ENABLE               = 1
TORQUE_DISABLE              = 0
DXL_MINIMUM_POSITION_VALUE  = 10
DXL_MAXIMUM_POSITION_VALUE  = 4000
DXL_MOVING_STATUS_THRESHOLD = 20

portHandler = PortHandler(DEVICENAME)
packetHandler = PacketHandler(PROTOCOL_VERSION)
portHandler.openPort()
portHandler.setBaudRate(BAUDRATE)

dxl_comm_result, dxl_error = packetHandler.write1ByteTxRx(portHandler, DXL_ID, ADDR_AX_TORQUE_ENABLE, TORQUE_ENABLE)
for i in range(1000):
    t = time.time()
    dxl_present_position, dxl_comm_result, dxl_error = packetHandler.read2ByteTxRx(portHandler, DXL_ID, ADDR_AX_PRESENT_POSITION)
    dxl_present_temp, dxl_comm_result, dxl_error = packetHandler.read1ByteTxRx(portHandler, DXL_ID, ADDR_AX_PRESENT_TEMPERATURE)
    print dxl_present_temp
    dxl_comm_result, dxl_error = packetHandler.write4ByteTxRx(portHandler, DXL_ID, ADDR_AX_GOAL_POSITION, int(512+100*math.sin(2*math.pi*0.5*t)))
    while time.time()-t<0.01:
        time.sleep(0.001)
dxl_comm_result, dxl_error = packetHandler.write1ByteTxRx(portHandler, DXL_ID, ADDR_AX_TORQUE_ENABLE, TORQUE_DISABLE)

portHandler.closePort()