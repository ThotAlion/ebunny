#!/usr/bin/python
# -*- coding: utf-8 -*-

import pypot.robot
import time
import math
import sys
import threading
import time
import Queue

from gpiozero import Button
import smbus

# part for gyro

# Register
power_mgmt_1 = 0x6b
power_mgmt_2 = 0x6c

def read_byte(reg):
    return bus.read_byte_data(address, reg)
 
def read_word(reg):
    h = bus.read_byte_data(address, reg)
    l = bus.read_byte_data(address, reg+1)
    value = (h << 8) + l
    return value
 
def read_word_2c(reg):
    val = read_word(reg)
    if (val >= 0x8000):
        return -((65535 - val) + 1)
    else:
        return val
 
def dist(a,b):
    return math.sqrt((a*a)+(b*b))
 
def get_y_rotation(x,y,z):
    radians = math.atan2(x, dist(y,z))
    return -math.degrees(radians)
 
def get_x_rotation(x,y,z):
    radians = math.atan2(y, dist(x,z))
    return math.degrees(radians)

def add_input(input_queue):
    while True:
        input_queue.put(sys.stdin.read(1))

def interp(a, x1, x2):
    return x1+a*(x2-x1)


def interpInv(x, x1, x2):
    return (x-x1)/(x2-x1)


def MGD(theta2):
    #print "theta2:"+str(theta2*180.0/math.pi)
    c = math.cos(theta2)
    s = math.sin(theta2)
    xA = 0.025
    yA = 0.045
    xB = 0.095
    yB = 0.000
    L2 = 0.130
    L3 = 0.055
    L4 = 0.122
    L5 = 0.140
    xC = xB+L2*c
    yC = yB+L2*s
    AC = math.sqrt((xA-xC)**2+(yA-yC)**2)
    #print "AC:"+str(AC)
    AH = min((L4**2-L3**2+AC**2)/(2*AC),L4)
    #print "AH:"+str(AH)
    HD = math.sqrt(L4**2-AH**2)
    #print "HD:"+str(HD)
    xH = xA+AH*(xC-xA)/AC
    yH = yA+AH*(yC-yA)/AC
    xD = xH-HD*(yC-yA)/AC
    yD = yH+HD*(xC-xA)/AC
    xF = xC+L5*(xC-xD)/L3
    yF = yC+L5*(yC-yD)/L3

    return math.atan((yF-yC)/(xF-xC))*180.0/math.pi, math.atan(yF/xF)*180.0/math.pi


nugget = pypot.robot.from_json('e_nugget.json')

right_foot = Button(15)

left_foot = Button(14)

ILS = Button(21)

bus = smbus.SMBus(1) # bus = smbus.SMBus(0) fuer Revision 1
address = 0x68       # via i2cdetect
# Aktivieren, um das Modul ansprechen zu koennen
bus.write_byte_data(address, power_mgmt_1, 0)



alpha = 0  # positif quand on ecarte
theta = 0  # negatif vers l'avant
aLc = 0  # repos à -40, extension à 30
aRc = 0  # repos à -40, extension à 30
compliant = True
speed = 100
state = -1
xLeft=0
xRight=0
KP = 10
KI = 5
rythme=1
srythme=10

input_queue = Queue.Queue()

input_thread = threading.Thread(target=add_input, args=(input_queue,))
input_thread.daemon = True
input_thread.start()
count = 0

last_update = time.time()

t0 = time.time()
t1 = time.time()

while True:
    if not input_queue.empty():
        c = input_queue.get()
        #print "\ninput:", c
        if c=='q':
            break
        if c=='a':
            state = -1

    # mesures
    # mesure de la temperature
    temp = 0
    for mot in nugget.motors:
        temp = max(temp, mot.present_temperature)
    if temp >60:
        print "HOT!"
        state = -1
    # mesure de l'angle quadrilatere
    aLm = interpInv(nugget.l_knee_y.present_position, -40, 30)
    aRm = interpInv(nugget.r_knee_y.present_position, -40, 30)
    try:
        gyroskop_xout = read_word_2c(0x43)
        roll = round((gyroskop_xout+830.0)/200.0)
    except:
        pass

    try:
        beschleunigung_zout = read_word_2c(0x3f)
        beschleunigung_xout = (read_word_2c(0x3b)-3000)/100
    except:
        pass

    if beschleunigung_zout<5000:
        count+=1
    else:
        count=0
    if count >= 100:
        state = -1

    print str(right_foot.is_pressed)+"\t"+str(left_foot.is_pressed)+"\t"+str(ILS.is_pressed)+"\t"+str(temp)+'°C\t'+str(roll)+"\t"+str(state)+'\t'+str(beschleunigung_xout)

    # machine a etat
    if state == 0:
        alpha = 25
        theta = 0
        aLc = 0.0
        aRc = 0.0
        speed = 20
        compliant = False
        if time.time()-t0 > 2:
            t0 = time.time()
            state = 1

    elif state == 1:
        alpha = 25
        theta = 0
        aLc = 0.3
        aRc = 0.3
        speed = 20
        compliant = False
        if ILS.is_pressed==False:
            t0 = time.time()
            t1 = time.time()
            state = 2

    elif state == 2:
        alpha = 25
        theta = 0
        aLc = 0.3
        aRc = 0.2
        speed = 100
        compliant = False
        if left_foot.is_pressed == False:
            t0 = time.time()
            state = 3
    
    elif state == 3:
        alpha = 25
        theta = 0
        aLc = 0.2
        aRc = 0.3
        speed = 100
        compliant = False
        if roll<-10:
            t0 = time.time()
            state = 4

    elif state == 4:
        alpha = 25
        theta = 0
        aLc = 0.3
        aRc = 0.3
        speed = 100
        compliant = False
        if left_foot.is_pressed == True:
            t0 = time.time()
            state = 5

    elif state == 5:
        alpha = 30
        theta = 0
        aLc = 0.3
        aRc = 0.1
        speed = 100
        compliant = False
        if roll>10:
            t0 = time.time()
            state = 6
    
    elif state == 6:
        alpha = 35
        theta = -5
        aLc = 0.3
        aRc = 0.3
        speed = 100
        compliant = False
        if right_foot.is_pressed == True:
            t0 = time.time()
            state = 7
    
    elif state == 7:
        alpha = 30
        theta = 0
        aLc = 0.1
        aRc = 0.3
        speed = 100
        compliant = False
        if roll<-10:
            t0 = time.time()
            state = 4
        if time.time()-t1 > 4*60:
            state = -1

    # elif state == 2:
    #     alpha = 20
    #     theta = 0
    #     aLc = 0.5
    #     aRc = 0.7
    #     speed = 2
    #     compliant = False
    #     if time.clock()-t0 > 5:
    #         t0 = time.clock()
    #         state = 3
            
    # elif state == 3:
    #     alpha = 20
    #     theta = 5
    #     aLc = 0.5
    #     aRc = 0.5
    #     speed = 100
    #     compliant = False
    #     if not left_foot.is_pressed and right_foot.is_pressed:
    #         t0 = time.clock()
    #         state = 4
            
    # elif state == 4:
    #     alpha = 30
    #     theta = 0
    #     aLc = 0.2
    #     aRc = 0.5
    #     speed = 100
    #     compliant = False
    #     if roll<-5:
    #         t0 = time.clock()
    #         state = 5
        
            
    # elif state == 5:
    #     alpha = 20
    #     theta = -5
    #     aLc = 0.5
    #     aRc = 0.5
    #     speed = 100
    #     compliant = False
    #     if not right_foot.is_pressed and left_foot.is_pressed:
    #         t0 = time.clock()
    #         state = 6
    
    # elif state == 6:
    #     alpha = 30
    #     theta = 0
    #     aLc = 0.5
    #     aRc = 0.2
    #     speed = 100
    #     compliant = False
    #     if roll>5:
    #         t0 = time.clock()
    #         state = 3

    elif state == -1:
        alpha = 25
        theta = 0
        aLc = 0.5
        aRc = 0.5
        speed = 100
        compliant = True
        if ILS.is_pressed:
            t0 = time.time()
            state = 0


    # actionneurs
    (aFr,lFr) = MGD((70-nugget.r_knee_y.present_position)*math.pi/180.0)
    (aFl,lFl) = MGD((70-nugget.l_knee_y.present_position)*math.pi/180.0)
    nugget.r_hip_x.pid = (KP,KI,0)
    nugget.r_hip_x.compliant = compliant
    nugget.r_hip_x.goal_position = alpha/2
    nugget.r_hip_x.moving_speed = 0
    
    nugget.l_hip_x.pid = (KP,KI,0)
    nugget.l_hip_x.compliant = compliant
    nugget.l_hip_x.goal_position = alpha/2
    nugget.l_hip_x.moving_speed = 0
    
    nugget.r_hip_y.compliant = compliant
    nugget.r_hip_y.goal_position = -lFr-theta/2
    nugget.r_hip_y.moving_speed = 0
    
    nugget.l_hip_y.compliant = compliant
    nugget.l_hip_y.goal_position = -lFl+theta/2
    nugget.l_hip_y.moving_speed = 0
    
    nugget.r_knee_y.pid = (KP,KI,0)
    nugget.r_knee_y.compliant = compliant
    nugget.r_knee_y.goal_position = interp(aRc, -40, 30)
    nugget.r_knee_y.moving_speed = speed
    
    nugget.l_knee_y.pid = (KP,KI,0)
    nugget.l_knee_y.compliant = compliant
    nugget.l_knee_y.goal_position = interp(aLc, -40, 30)
    nugget.l_knee_y.moving_speed = speed
    
    nugget.r_ankle_y.compliant = compliant
    nugget.r_ankle_y.goal_position = aFr-lFr-0
    nugget.r_ankle_y.moving_speed = 0
    
    nugget.l_ankle_y.compliant = compliant
    nugget.l_ankle_y.goal_position = aFl-lFl-0
    nugget.l_ankle_y.moving_speed = 0

    time.sleep(0.005)
for mot in nugget.motors:
    mot.compliant = True
time.sleep(0.5)
nugget.close()
